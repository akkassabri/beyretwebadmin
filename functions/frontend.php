<?php
require 'config.php';
require 'backend.php';

function getBeyretLink(){
    return "https://beyretdijital.com/";
}

function getBeyret(){
    return "Beyret Dijital";
}

function getCompanyLogo(){
    $row = getCompanyInfo();
    return $row['companyLogo'];
}

function getCompanyName(){
    $row = getCompanyInfo();
    return $row['companyName'];
}

/*SOSYAL MEDYA BAŞLANGIÇ*/
function getFacebook(){
    $row = getSocialLinks();
    return $row['facebook'];
}

function getInstagram(){
    $row = getSocialLinks();
    return $row['instagram'];
}

function getLinkedin(){
    $row = getSocialLinks();
    return $row['linkedin'];
}

function getGoogle(){
    $row = getSocialLinks();
    return $row['googleplus'];
}

function getTwitter(){
    $row = getSocialLinks();
    return $row['twitter'];
}
/*SOSYAL MEDYA BİTİŞ*/
/*İLETİŞİM BAŞLANGIÇ*/
function getAddress(){
    $row = getContactInfo();
    return $row['adress'];
}

function getPhone(){
    $row = getContactInfo();
    return $row['phone'];
}

function getPhone2(){
    $row = getContactInfo();
    return $row['phone2'];
}

function getGsm(){
    $row = getContactInfo();
    return $row['gsm'];
}

function getEmail(){
    $row = getContactInfo();
    return $row['email'];
}

function getMap(){
    $row = getContactInfo();
    return $row['maplink'];
}
/*İLETİŞİM BİTİŞ*/
function getAbout(){
    $row = getAboutUs();
    return $row['about'];
}

function getServices(){
    $query = "Select * from services where status=1";
    $result = run($query);
    return $result;
}

function getServiceWithSlug($slug){
    $query = "Select * from services where slug='$slug' and status=1";
    $result=run($query);
    return $result;
}

function getServiceKeywordsWithId($id){
    $list=getServiceKeywords($id);
    $keywords="";
    foreach ($list as $key) {
        $keywords=$keywords.$key["keyword"].",";
    }
    return rtrim($keywords,", ");
}

function getBlogs($limit=999){
    $query = "Select * from blogs where post_status=1 order by id desc LIMIT $limit ";
    $result = run($query);
    return $result;
}

function getBlogWithSlug($slug){
    $query = "Select * from blogs where post_slug='$slug' and post_status=1";
    $result=run($query);
    return $result;
}

function getBlogKeywordsWithId($id){
    $list=getBlogKeywords($id);
    $keywords="";
    foreach ($list as $key) {
        $keywords=$keywords.$key["keyword"].",";
    }
    return rtrim($keywords,", ");
}

?>
