<?php
session_start();
if(!isset($_SESSION['UserID'])){
    header("location:login.php");
}
?>
<?php
require_once './functions/backend.php';
checkAccess(basename(__FILE__));
if (isset($_POST['submit'])) {
    $name=$_POST['name'];
    $price=$_POST['price'];
    $info=$_POST['info'];
    $keywords=$_POST['keywords'];
    $title=$_POST['title'];
    $description=$_POST['description'];
    $alt=$_POST['alt'];

    addProduct($name,$info,$price,$keywords,$title,$description,$alt);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include './includes/head.php'; ?>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary">
<?php include './includes/header.php'; ?>
<?php include './includes/leftmenu.php'; ?>

<main id="app-main" class="app-main">
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">Yeni Ürün Ekle</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form class="form-horizontal" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label for="name" class="col-sm-3 control-label">Ürün Adı:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" required placeholder="Ürün Adını Giriniz">
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label for="price" class="col-sm-3 control-label">Ürün Fiyatı:</label>
                                  <div class="col-sm-9">
                                    <input type="text" class="form-control" name="price" placeholder="Ürün Fiyatını Giriniz">
                                  </div>
                                </div>
                                <div class="form-group">
                                    <label for="info" class="col-sm-3 control-label">Açıklama:</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="info" placeholder="Ürün Açıklamasını Giriniz"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="info" class="col-sm-3 control-label">Anahtar Kelimeler:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="keywords" placeholder="Anahtar Kelimeleri Giriniz">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="info" class="col-sm-3 control-label">Ürünün Sayfa Açıklaması:</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="description" placeholder="Ürün Sayfa Açıklamasını Giriniz"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="info" class="col-sm-3 control-label">Ürünün Sayfa Başlığı:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="title" placeholder="Ürünün Sayfa Başlığını Giriniz">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="picture" class="col-sm-3 control-label">Ürün Resmi:</label>
                                    <div class="col-sm-9">
                                        <input type="file" id="picture" name="img" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="info" class="col-sm-3 control-label">Resmin Açıklaması</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="alt" placeholder="Resmin Açıklamasını Giriniz">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" name="submit" class="btn btn-success">Ekle</button>
                                    </div>
                                </div>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- END column -->

        </section><!-- #dash-content -->
    </div>
    <?php include './includes/footer.php'; ?>
</main>

<?php include("./includes/foot.php") ?>
</body>
</html>
