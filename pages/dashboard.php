<?php
if(!isset($_SESSION)){
    session_start();
}
if(!isset($_SESSION['UserID'])){
    header("location:../login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include '../includes/head.php'; ?>
	</head>
	<body class="menubar-left menubar-unfold menubar-light theme-primary">
		<?php include '../includes/header.php'; ?>
		<?php include '../includes/leftmenu.php'; ?>
		<main id="app-main" class="app-main">
		  <div class="wrap">
		    <section class="app-content">
		    </section><!-- #dash-content -->
		  </div>
		  <?php include '../includes/footer.php'; ?>
		</main>
		<?php include("../includes/foot.php") ?>
	</body>
</html>
