<?php
if(!isset($_SESSION)){
    session_start();
}
if(!isset($_SESSION['UserID'])){
    header("location:../login.php");
}
?>
<?php
require_once '../functions/backend.php';
checkAccess(basename(__FILE__));
$gallery=getGalleryImages();

if (isset($_POST['submit'])) {
   saveGalleryImage();
   echo "<meta http-equiv='refresh' content='0'>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include '../includes/head.php'; ?>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary">
<?php include '../includes/header.php'; ?>
<?php include '../includes/leftmenu.php'; ?>

<main id="app-main" class="app-main">
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                    <div class="col-md-12">
                        <div class="mail-toolbar m-b-lg">
                            <div class="btn-group" role="group">
                                <label for="logo" class="col-sm-2 control-label">Yeni Resim</label>
                                <div class="col-sm-8">
                                    <input type="file"  class="form-control" name="files[]" multiple>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="submit" class="btn btn-success">Kaydet</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                <div id="gallery" class="gallery m-b-lg">
                    <div class="row">
                        <?php
                        while ( $row = $gallery->fetch_assoc()){
                        ?>
                        <div class="col-xs-6 col-sm-4 col-md-3" >
                            <div class="gallery-item">
                                <img style="height:200px !important;" class="img-responsive" src="../assets/images/gallery/<?php echo $row['source']; ?>">
                                <button type="button" onclick="test(<?php echo $row['id']; ?>)" class="btn btn-danger"><i class="fa fa-remove"></i></button>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </form>

        </section><!-- #dash-content -->
    </div>
    <?php include '../includes/footer.php'; ?>
</main>

<?php include("../includes/foot.php") ?>
<script>

    function test(e) {
        var result = confirm("Bu fotoğrafı silmek istediğinize emin misiniz ?");
        if (result) {
            window.location.href = "../functions/backend.php?deleteImageId="+e;
        }
    }
</script>
</body>
</html>
