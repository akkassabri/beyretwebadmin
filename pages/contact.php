<?php
if(!isset($_SESSION)){
    session_start();
}
if(!isset($_SESSION['UserID'])){
    header("location:../login.php");
}
?>
<?php
require_once '../functions/backend.php';
checkAccess(basename(__FILE__));
$contact=getContactInfo();

if (isset($_POST['submit'])) {
    $tel1=$_POST['tel1'];
    $tel2=$_POST['tel2'];
    $gsm=$_POST['gsm'];
    $email=$_POST['email'];
    $map=$_POST['map'];
    $adres=$_POST['adres'];
    saveContactInfo($tel1,$tel2,$gsm,$email,$map,$adres);
    echo "<meta http-equiv='refresh' content='0'>";
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include '../includes/head.php'; ?>
	</head>
	<body class="menubar-left menubar-unfold menubar-light theme-primary">
		<?php include '../includes/header.php'; ?>
		<?php include '../includes/leftmenu.php'; ?>

		<main id="app-main" class="app-main">
		  <div class="wrap">
		    <section class="app-content">
		    	<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">İletişim Bilgileri</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<form class="form-horizontal" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
							<div class="form-group">
								<label for="tel1" class="col-sm-3 control-label">Telefon 1:</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="tel1" placeholder="Telefon" value="<?php echo $contact['phone']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="tel2" class="col-sm-3 control-label">Telefon 2:</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="tel2" placeholder="Telefon" value="<?php echo $contact['phone2']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="gsm" class="col-sm-3 control-label">Gsm:</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="gsm" placeholder="Gsm" value="<?php echo $contact['gsm']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-sm-3 control-label">Email:</label>
								<div class="col-sm-9">
									<input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo $contact['email']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="map" class="col-sm-3 control-label">Google Maps Linki:</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="map" placeholder="Maps Link" value="<?php echo $contact['maplink']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="adres" class="col-sm-3 control-label">Adres Bilgileri:</label>
								<div class="col-sm-9">
									<textarea class="form-control" name="adres" placeholder="Adres..."><?php echo $contact['adress']; ?></textarea>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-9 col-sm-offset-3">
									<button type="submit" name="submit" class="btn btn-success">Kaydet</button>
								</div>
							</div>
						</form>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
		    </section><!-- #dash-content -->
		  </div>
		  <?php include '../includes/footer.php'; ?>
		</main>

		<?php include("../includes/foot.php") ?>
	</body>
</html>
