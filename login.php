<?php
require './config.php';
if(!isset($_SESSION)){
    session_start();
}
if(isset($_SESSION['UserID'])){
header("location: pages/dashboard.php");
}
require_once './functions/backend.php';
$company=getCompanyInfo();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title><?php echo $company['companyName']; ?> Yönetim Paneli</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta name="description" content="Admin, Dashboard, Bootstrap" />
		<link rel="stylesheet" href="./libs/bower/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="./libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
		<link rel="stylesheet" href="./libs/bower/animate.css/animate.min.css">
		<link rel="stylesheet" href="./assets/css/bootstrap.css">
		<link rel="stylesheet" href="./assets/css/core.css">
		<link rel="stylesheet" href="./assets/css/misc-pages.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300">
	</head>
	<body class="simple-page">
		<div class="simple-page-wrap">
			<div class="simple-page-form animated flipInY" id="login-form">
				<h4 class="form-title m-b-xl text-center"><?php echo $company['companyName']; ?> Yönetim Paneli Giriş</h4>
				<form action="functions/admin_model.php" method="POST">
					<div class="form-group">
						<input id="sign-in-email" type="email" name="email" class="form-control" placeholder="E-posta ">
					</div>
					<div class="form-group">
						<input id="sign-in-password" type="password" name="password" class="form-control" placeholder="Şifre">
					</div>
					<input type="submit" class="btn btn-primary" name="submit" value="Giriş Yap">
				</form>
			</div><!-- #login-form -->
			<div class="simple-page-footer">
				<p><a href="https://beyretdijital.com/">Şifrenizi Unuttuysanız Bizimle İletişime Geçebilirsiniz</a></p>
			    <p><a href="https://beyretdijital.com/" target="_blank"> Copyright Beyret Dijital 2018 &copy;</a></p>
			</div><!-- .simple-page-footer -->
		</div><!-- .simple-page-wrap -->
	</body>
</html>
