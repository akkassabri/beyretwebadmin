<?php

if(!isset($_SESSION)){
    session_start();
}
if(!isset($_SESSION['UserID'])){
    header("location:login.php");
}else{
    header("location:pages/dashboard.php");
}

?>