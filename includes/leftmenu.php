
<aside id="menubar" class="menubar light">
    <div class="menubar-scroll">
    <div class="menubar-scroll-inner">
      <ul class="app-menu">
          <?php $list=getActiveModules();
          while($row = $list->fetch_assoc()) {
              if($row['type']=="main"){
                  echo '<li>';
                  echo '<a href="'.$row['href'].'">';
                  echo '<i class="menu-icon zmdi zmdi-'.$row['icon'].' zmdi-hc-lg"></i>';
                  echo '<span class="menu-text">'.$row['title'].'</span>';
                  echo '</a>';
                  echo '</li>';
              }elseif($row['type']=="parent"){
                  if (getSubmenus($row['id'])){
                      echo '<li class="has-submenu">';
                  echo '<a href="javascript:void(0)" class="submenu-toggle">';
                  echo '<i class="menu-icon zmdi zmdi-'.$row['icon'].' zmdi-hc-lg"></i>';
                  echo '<span class="menu-text">'.$row['title'].'</span>';
                  echo '<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>';
                  echo '</a>';
                  echo '<ul class="submenu">';
                  $submenus=getSubmenus($row['id']);
                  while ($subrow = $submenus->fetch_assoc()){
                      echo '<li><a href="'.$subrow['href'].'"><span class="menu-text">'.$subrow['title'].'</span></a></li>';
                  }
               echo '</ul>';
           echo '</li>';
                  }
              }
          }
          ?>
        <li class="menu-separator"><hr></li>
        <li><a href="../functions/admin_model.php?do=logout"><i class="zmdi m-r-md zmdi-hc-lg zmdi-account-box"></i>Çıkış Yap</a></li>
      </ul><!-- .app-menu -->
    </div><!-- .menubar-scroll-inner -->
  </div><!-- .menubar-scroll -->
</aside>
