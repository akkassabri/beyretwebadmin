<?php
require_once '../functions/backend.php';
$company=getCompanyInfo();
?>
<nav id="app-navbar" class="navbar navbar-inverse navbar-fixed-top primary">
  <div class="navbar-header">
    <button type="button" id="menubar-toggle-btn" class="navbar-toggle visible-xs-inline-block navbar-toggle-left hamburger hamburger--collapse js-hamburger">
      <span class="sr-only">Toggle navigation</span>
      <span class="hamburger-box"><span class="hamburger-inner"></span></span>
    </button>
    <a href="dashboard.php" class="navbar-brand">
      <span class="brand-icon"><i class="fa fa-gg"></i></span>
      <span class="brand-name"><?php echo $company['companyName']; ?> Yönetim Paneli</span>
    </a>
  </div><!-- .navbar-header -->
</nav>
